using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class End : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
      UnityEngine.Video.VideoPlayer videoPlayer = GetComponent<UnityEngine.Video.VideoPlayer>();
      videoPlayer.url = System.IO.Path.Combine(Application.streamingAssetsPath,"Video.mp4");
      videoPlayer.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("escape") || !GetComponent<UnityEngine.Video.VideoPlayer>().isPlaying)
        {
            Application.Quit();
        }
    }
}
