using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
  public float collectDistance = 0.005f;
  public Transform playerTransform;
  public Player player;

  private Vector3 distanceToPlayer = Vector3.zero;


    // Start is called before the first frame update
  void Start()
  {

  }

  // Update is called once per frame
  void FixedUpdate() {
    if (GetComponent<BoxCollider2D>().IsTouching(player.GetComponent<BoxCollider2D>())) {
      GetCollected();
    }
  }

  void GetCollected() {
    player.CollectObject();
    Debug.Log("je me détruis");
    Destroy(gameObject);
  }
}
