using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Npc : MonoBehaviour
{

  public int id;
  public DialogsManager dialogsManager;

  public LayersController layersController;
  private int currentLayer;

  private int previousLayer;


  public Transform player;

  public GameObject dialogBox;
  public UnityEngine.UI.Text text;

  public GameObject meanBox;
  public UnityEngine.UI.Text meanText;

  public float talkingDistance = 8f;

  private bool talking = false;

  private Vector3 distanceToPlayer = Vector3.zero;

  private Animator animator;
  private SpriteRenderer spriteRenderer;

  private bool comingToYou = false;

  private Rigidbody2D rb;

  private bool flipped = false;

  public bool nextToYou = false;

  void Start()
  {
    dialogBox.SetActive(false);
  }

  // Update is called once per frame
  void Update()
  {

  }

  public void ComeToYou() {
    if (id == 0) {
      comingToYou = true;
    }
    if(!flipped) {
      Flip();
      flipped = true;
    }
  }

  public void Flip() {
    Vector3 theScale = transform.localScale;
    theScale.x *= -1;
    transform.localScale = theScale;
  }

  public Animator GetAnimator() {
    return animator;
  }

  void GetCurrentAnimator() {
    if (animator != null) {
      animator.enabled = false;
    }
    if (spriteRenderer != null) {
      spriteRenderer.enabled = false;
    }
    if(currentLayer > 4)
      currentLayer = 4;

    if(id > 0) {
      animator = transform.Find($"NPC{id}").transform.Find($"Layer{currentLayer}").GetComponent<Animator>();
      spriteRenderer = transform.Find($"NPC{id}").transform.Find($"Layer{currentLayer}").GetComponent<SpriteRenderer>();
      spriteRenderer.enabled = true;
      animator.enabled = true;
    } else {
      animator = transform.Find($"NPC{id}").GetComponent<Animator>();
      spriteRenderer = transform.Find($"NPC{id}").GetComponent<SpriteRenderer>();
      spriteRenderer.enabled = true;
      animator.enabled = true;
    }
  }

  void Awake() {
    currentLayer = layersController.currentLayer;
    GetCurrentAnimator();
    if(id == 0) {
      rb = GetComponent<Rigidbody2D>();
    }
  }

  void FixedUpdate() {
    currentLayer = layersController.currentLayer;
    if (currentLayer != previousLayer) {
      GetCurrentAnimator();
      previousLayer = currentLayer;
    }
    distanceToPlayer = (player.position - transform.position);
    if (id != 0 || (!comingToYou && !nextToYou)) {
      if (distanceToPlayer.magnitude < talkingDistance && (!talking || layersController.currentLayer != layersController.previousLayer)) {
        Speak();
		  } else if (distanceToPlayer.magnitude >= talkingDistance && talking) {
        ShutUp();
      }
    } else if (id == 0 && comingToYou){
      if(distanceToPlayer.magnitude > 2.0) {
        if (!animator.GetBool("Walk")) {
          animator.SetBool("Walk", true);
        }
        Vector2 targetVelocity = new Vector2(Vector3.left.x * 2.2f, 0);
        rb.velocity = targetVelocity;
      } else {
        animator.SetBool("Walk", false);
        rb.velocity = Vector3.zero;
        nextToYou = true;
        comingToYou = false;
      }
    } else {
      animator.SetBool("Walk", false);
      rb.velocity = Vector3.zero;
      nextToYou = true;
      comingToYou = false;
    }

  }

  void Speak()
  {
    Debug.Log("PNJ Speaking");
    if (layersController.currentLayer<4 || id==0)
    {
      text.font = dialogsManager.fontRegular;
      text.text = dialogsManager.DisplayDialog(id);
      meanBox.SetActive(false);
    }
    else
    {
      text.text = "";
      meanText.font = dialogsManager.fontEvil;
      meanBox.SetActive(true);
      meanText.text = dialogsManager.DisplayDialog(id);
    }
    dialogBox.SetActive(true);
    talking = true;
  }

  void ShutUp() {
    Debug.Log("je me tais");
    talking = false;
    dialogBox.SetActive(false);
  }
}

