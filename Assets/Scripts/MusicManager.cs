using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
  public LayersController layersController;
  public TrackAudioClip[] MusicArray;
  private List<AudioSource> audioSources = new List<AudioSource>();
  private Coroutine latestCoroutine;
  private Track currentTrack;

  [System.Serializable]
  public class TrackAudioClip
  {
    public MusicManager.Track track;
    public AudioClip audioClip;
  }

  public enum Track
  {
    happy,
    ok,
    low,
    lower,
    lowest,
    death
  }

  void Awake()
  {
    currentTrack = Track.ok;

    GameObject trackHappy = new GameObject($"happy");
    AudioSource audioSourceHappy = trackHappy.AddComponent<AudioSource>();
    audioSourceHappy.transform.SetParent(transform);
    audioSourceHappy.clip = GetAudioClip(Track.happy);
    audioSourceHappy.volume = 0.0f;
    audioSourceHappy.Play();

    GameObject trackOk = new GameObject($"ok");
    AudioSource audioSourceOK = trackOk.AddComponent<AudioSource>();
    audioSourceOK.transform.SetParent(transform);
    audioSourceOK.clip = GetAudioClip(Track.ok);
    audioSourceOK.volume = 1.0f;
    audioSourceOK.Play();

    GameObject trackLow = new GameObject($"low");
    AudioSource audioSourceLow = trackLow.AddComponent<AudioSource>();
    audioSourceLow.transform.SetParent(transform);
    audioSourceLow.clip = GetAudioClip(Track.low);
    audioSourceLow.volume = 0.0f;
    audioSourceLow.Play();

    GameObject trackLower = new GameObject($"lower");
    AudioSource audioSourceLower = trackLower.AddComponent<AudioSource>();
    audioSourceLower.transform.SetParent(transform);
    audioSourceLower.clip = GetAudioClip(Track.lower);
    audioSourceLower.volume = 0.0f;
    audioSourceLower.Play();

    GameObject trackLowest = new GameObject($"lowest");
    AudioSource audioSourceLowest = trackLowest.AddComponent<AudioSource>();
    audioSourceLowest.transform.SetParent(transform);
    audioSourceLowest.clip = GetAudioClip(Track.lowest);
    audioSourceLowest.volume = 0.0f;
    audioSourceLowest.Play();

    GameObject trackDeath = new GameObject($"death");
    AudioSource audioSourceDeath = trackDeath.AddComponent<AudioSource>();
    audioSourceDeath.transform.SetParent(transform);
    audioSourceDeath.clip = GetAudioClip(Track.death);
    audioSourceDeath.volume = 0.0f;
    audioSourceDeath.Play();
  }


  void FixedUpdate()
  {
    int prevLayer = layersController.previousLayer, nextLayer = layersController.currentLayer;
    if (prevLayer != nextLayer)
    {
      switch(nextLayer)
      {
        case 0:
          PlayTrack(Track.ok);
          break;
        case 1:
        case 2:
          PlayTrack(Track.low);
          break;
        case 3:
          PlayTrack(Track.lower);
          break;
        case 4:
          PlayTrack(Track.lowest);
          break;
        default:

          break;
      }
    }

    AudioSource audioSourceOK = transform.Find($"ok").GetComponent<AudioSource>();
    if(audioSourceOK.time>=74.67f)
    {
      LoopTracks();
    }
  }

  public void PlayTrack(Track track)
  {
    if(track==currentTrack)
    {
      return;
    }
    AudioSource currentSource = transform.Find($"{currentTrack}").GetComponent<AudioSource>();
    AudioSource nextSource = transform.Find($"{track}").GetComponent<AudioSource>();
    if(latestCoroutine!=null)
    {
      StopCoroutine(latestCoroutine);
      MuteAll();
    }
    latestCoroutine = StartCoroutine(AudioFadeOut.CrossFade(currentSource, nextSource, 0.5f));
    currentTrack = track;
  }

  private AudioClip GetAudioClip(Track track)
  {
    foreach (TrackAudioClip soundAudioClip in MusicArray)
    {
      if (soundAudioClip.track == track)
      {
        return soundAudioClip.audioClip;
      }
    }
    Debug.LogError("Ca a merd� frer: son " + track + " introuvable");
    return null;
  }


  private void LoopTracks()
  {
    AudioSource audioSourceOK = transform.Find($"ok").GetComponent<AudioSource>();
    AudioSource audioSourceLow = transform.Find($"low").GetComponent<AudioSource>();
    AudioSource audioSourceLower = transform.Find($"lower").GetComponent<AudioSource>();
    AudioSource audioSourceLowest = transform.Find($"lowest").GetComponent<AudioSource>();
    AudioSource audioSourceDeath = transform.Find($"death").GetComponent<AudioSource>();
    audioSourceOK.time = 14.930f;
    audioSourceLow.time = 14.930f;
    audioSourceLower.time = 14.930f;
    audioSourceLowest.time = 14.930f;
    audioSourceDeath.time = 14.930f;
  }


  private void MuteAll()
  {
    AudioSource audioSourceOK = transform.Find($"ok").GetComponent<AudioSource>();
    AudioSource audioSourceLow = transform.Find($"low").GetComponent<AudioSource>();
    AudioSource audioSourceLower = transform.Find($"lower").GetComponent<AudioSource>();
    AudioSource audioSourceLowest = transform.Find($"lowest").GetComponent<AudioSource>();
    AudioSource audioSourceDeath = transform.Find($"death").GetComponent<AudioSource>();
    audioSourceOK.volume = 0f;
    audioSourceLow.volume = 0f;
    audioSourceLower.volume = 0f;
    audioSourceLowest.volume = 0f;
    audioSourceDeath.volume = 0f;
  }
}
