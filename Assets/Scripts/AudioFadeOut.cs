using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AudioFadeOut
{

  public static IEnumerator FadeOut(AudioSource audioSource, float FadeTime)
  {
    float startVolume = audioSource.volume;

    while (audioSource.volume > 0)
    {
      audioSource.volume -= startVolume * Time.deltaTime / FadeTime;

      yield return null;
    }

    audioSource.Stop();
    audioSource.volume = startVolume;
  }

  public static IEnumerator FadeIn(AudioSource audioSource, float FadeTime, float endVolume)
  {
    float startVolume = 0.2f;

    while (audioSource.volume < endVolume)
    {
      audioSource.volume += startVolume * 2.0f * Time.deltaTime / FadeTime;

      yield return null;
    }

    audioSource.volume = endVolume;
  }


  public static IEnumerator CrossFade(AudioSource audioSourceFrom, AudioSource audioSourceTo, float FadeTime)
  {
    float startVolumeFrom = audioSourceFrom.volume;
    float startVolumeTo = 0.2f;
    while (audioSourceTo.volume < 1f)
    {
      audioSourceTo.volume += startVolumeTo * Time.deltaTime / FadeTime;
      audioSourceFrom.volume -= startVolumeFrom * Time.deltaTime / FadeTime;

      yield return null;
    }

    audioSourceFrom.volume = 0f;
    audioSourceTo.volume = 1f;
  }

}