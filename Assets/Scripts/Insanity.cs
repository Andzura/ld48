using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Insanity : MonoBehaviour
{
  public int insanityLevel = 50;
  private int maxInsanityLevel = 100;
  // Start is called before the first frame update
  public LayersController layerController;


  private int currentLayer;

  private float insanityIncreaseStep = 3f;
  private float insanityDecreaseStep = 0.01f;

  private float time = 0f;

  private float haltedRemainingTime = 0f;
  private float haltedTime = 10f;

  private Dictionary<int, int> upRatio = new Dictionary<int, int>();

  void Awake() {
    upRatio.Add(0, 1);
    upRatio.Add(1, 2);
    upRatio.Add(2, 3);
    upRatio.Add(3, 4);
    upRatio.Add(4, 5);
    upRatio.Add(5, 0);
    upRatio.Add(6, 0);
  }

  void IncreaseInsanity() {
    insanityLevel += upRatio[currentLayer];
    if (insanityLevel > maxInsanityLevel) {
      insanityLevel = maxInsanityLevel;
    }
  }

  void DecreaseInsanity() {
    insanityLevel -= 1;
    if (insanityLevel < 0) {
      insanityLevel = 0;
    }
  }

  public void ResetInsanity() {
    insanityLevel = maxInsanityLevel/2;
  }

  void RegularInsanityIncrease() {
    time += Time.deltaTime;
    if (time >= insanityIncreaseStep) {
      IncreaseInsanity();
      time = 0f;
    }
  }

  void RegularInsanityDecrease() {
    time += Time.deltaTime;
    if (time >= insanityDecreaseStep) {
      DecreaseInsanity();
      time = 0f;
    }
  }

  void Start() {

  }

  // Update is called once per frame
  void FixedUpdate() {
    currentLayer = layerController.currentLayer;
    if(GetComponent<Player>().IsFocusing()) {
      RegularInsanityDecrease();
    } else {
      if(haltedRemainingTime > 0f) {
        haltedRemainingTime -= Time.deltaTime;
      } else {
        RegularInsanityIncrease();
      }
    }
    AutoLayerChange();
  }

  void AutoLayerChange() {
    if (insanityLevel == maxInsanityLevel) {
      layerController.goDeeper();
      GetComponent<Player>().ResetFocus();
    } else if (insanityLevel == 0) {
      layerController.goUp();
      GetComponent<Player>().ResetFocus();
    }
  }

  public bool HaltInsanity() {
    if (haltedRemainingTime > 0f ) {
      return false;
    }
    haltedRemainingTime = haltedTime;
    return true;
  }
}
