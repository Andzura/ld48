using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayersController : MonoBehaviour
{

  public int currentLayer = 0;
  public int previousLayer = 0;
  public Layer[] layers = new Layer[7];
  private float layoutChangeDelta = 0.75f;

  private float timeRemainingLayerChanger;
  // Start is called before the first frame update

  public Insanity insanity;

  void Start() {
    foreach (Transform child in transform) {
      Layer l = child.GetComponent<Layer>();
      if(l == null) {
        Debug.Log("Nik");
        continue;
      }
      int pos = l.layer_depth;
      layers[pos] = l;
    }
  }

  // Update is called once per frame
  void FixedUpdate() {
    if (timeRemainingLayerChanger > 0) {
      timeRemainingLayerChanger -= Time.deltaTime;
    }
  }

  public void goDeeper() {
    if(this.currentLayer < 4 && timeRemainingLayerChanger <= 0) {
      this.previousLayer = this.currentLayer;
      this.currentLayer++;
      this.StartDeeperFade();
      layers[this.previousLayer].DeActivate();
      layers[this.currentLayer].Activate();
      this.EndDeeperFade();
      insanity.ResetInsanity();
      timeRemainingLayerChanger = layoutChangeDelta;
    }
  }

  public void goUp() {
    if(this.currentLayer > 0  && timeRemainingLayerChanger <= 0) {
      this.previousLayer = this.currentLayer;
      this.currentLayer--;
      this.StartUpperFade();
      layers[this.previousLayer].DeActivate();
      layers[this.currentLayer].Activate();
      this.EndUpperFade();
      insanity.ResetInsanity();
      timeRemainingLayerChanger = layoutChangeDelta;
    }
  }

  public void TimeOut() {
    this.previousLayer = this.currentLayer;
    this.currentLayer = 5; // "death" layer
    this.StartTimeOutFade();
    layers[this.previousLayer].DeActivate();
    layers[this.currentLayer].Activate();
    this.EndTimeOutFade();
  }

  public void Respawn(){
    if(this.currentLayer == 5){
      this.currentLayer = this.previousLayer;
      this.previousLayer = 5;
      this.StartRespawnFade();
      layers[this.previousLayer].DeActivate();
      layers[this.currentLayer].Activate();
      this.EndRespawnFade();
    }
  }

  void StartUpperFade() {
    //Do something :shrug:
  }


  void EndUpperFade() {
    //Do something :shrug:
  }


  void StartDeeperFade() {
    //Do something :shrug:
  }

  void EndDeeperFade() {
    //Do something :shrug:
  }

  void StartTimeOutFade() {
    //Do something :shrug:
  }

  void EndTimeOutFade() {
    //Do something :shrug:
  }

  void StartRespawnFade() {
    //Do something :shrug:
  }

  void EndRespawnFade() {
    //Do something :shrug:
  }

}
