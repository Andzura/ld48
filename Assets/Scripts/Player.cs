using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
  public LayersController layersController;
  public CollectiblesManager collectiblesManager;
  public CheckpointsController checkpointsController;
  public MusicManager musicManager;
  public SoundManager soundManager;
  public CameraFollow camera;
  public float requiredFocusTime = 1f;
  private float waitDeathTime = 5f;
  private Animator animator;

  private CharacterController2D characterController2D;
  private Rigidbody2D rb;
  private InputController inputController;

  public Tutorial tutorial;
  private Insanity insanity;
  private bool focus;
  private bool focusing;
  private float focusTime;
  private bool dead = false;
  private bool didRespawn = false;
  private float deathTime = 0f;
  private Vector2 nextRespawn = Vector2.zero;

  void Awake(){
    rb = GetComponent<Rigidbody2D>();
    characterController2D = GetComponent<CharacterController2D>();
    inputController = gameObject.AddComponent<InputController>() as InputController;
    insanity = gameObject.AddComponent<Insanity>() as Insanity;
    collectiblesManager = gameObject.AddComponent<CollectiblesManager>() as CollectiblesManager;

    inputController.characterController = characterController2D;
    inputController.checkpointsController = checkpointsController;
    inputController.layersController = layersController;
    insanity.layerController = layersController;
    characterController2D.layersController = layersController;
    layersController.insanity = insanity;
    if (tutorial != null) {
      tutorial.inputController = inputController;
      tutorial.characterController = characterController2D;
      tutorial.layersController = layersController;

      inputController.allowUp = false;
      inputController.allowFocus = false;
    }
  }

  // Update is called once per frame
  void FixedUpdate() {
    if(!dead){
      if(focus){
        characterController2D.SetFocus(true);
        if (focusing) {
          focusTime = 0;
        } else {
          focusTime += Time.deltaTime;
          if (focusTime > requiredFocusTime) {
            focusTime = 0;
            focusing = true;
            Debug.Log("J'ai fini de focus, mon insanity commence à descendre !!");
          }
        }
      }else{
        ResetFocus();
      }
    }else{
      deathTime += Time.deltaTime;
      if(deathTime > waitDeathTime){
        Respawn();
      }else if(waitDeathTime - deathTime < 0.75f){
        SetRestrictedCamera(false);
        SetSnapCamera(false);
        //ANIM RESPAWN
      }else{
        if(!didRespawn){
          transform.position = nextRespawn;
          Debug.Log("Hello");
          didRespawn = true;
        }
        //ANIM DEATH
      }
    }
  }

  public void SetFocus(bool focus){
    this.focus = focus;
  }

  public void ResetFocus(){
    characterController2D.SetFocus(false);
    focusing = false;
    focus = false;
    focusTime = 0.0f;
  }
  public bool IsFocusing(){
    return focusing;
  }

  public void Die() {
    musicManager.PlayTrack(MusicManager.Track.death);
    soundManager.PlaySound(SoundManager.Sound.PlayerHit,false, false);
    rb.isKinematic = true;
    rb.velocity = Vector2.zero;
    dead = true;
    didRespawn = false;
    ResetFocus();
    inputController.BlockInput();
    nextRespawn = checkpointsController.getRespawnPoint(transform.position);
    layersController.TimeOut();
    SetRestrictedCamera(true);
    SetSnapCamera(true);
  }
  public void Respawn(){
    rb.isKinematic = false;
    deathTime = 0f;
    dead = false;
    inputController.AllowInput();
    layersController.Respawn();
  }

  public void CollectObject() {
    collectiblesManager.GetCollectible();
    soundManager.PlaySound(SoundManager.Sound.PlayerEatConsumable, false, false);
  }

  public void UseObject() {
    if (collectiblesManager.HasObject() && insanity.HaltInsanity()) {
      collectiblesManager.UseCollectible();
      soundManager.PlaySound(SoundManager.Sound.PlayerEatConsumable, false, false);
    }
  }

  public void SetSnapCamera(bool snap){
    if(snap){
      camera.smoothingFactor = 0f;
    }else {
      camera.smoothingFactor = 0.3f;
    }
  }
  public void SetRestrictedCamera(bool restricted){
    camera.overrideArea = restricted;
  }

  // called when this GameObject collides with GameObject2.
  void OnTriggerEnter2D(Collider2D col)
  {
      Debug.Log("TRIGGER");
      KillZone kz = col.gameObject.GetComponent<KillZone>();
      if(kz != null && kz.active){
        if(!characterController2D.hidden && !dead){
          Debug.Log("DIE");
          this.Die();
        }
      }

      TriggerCameraChange tcc = col.gameObject.GetComponent<TriggerCameraChange>();
      if(tcc != null){
        camera.playableArea = tcc.newArea;
      }
      LoadScene ls = col.gameObject.GetComponent<LoadScene>();
      if(ls != null){
        SceneManager.LoadScene(ls.nextScene, LoadSceneMode.Single);
      }
  }
}
