﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;

public class Dialog
{
  // Start is called before the first frame update
  public int currentLayer;

  public string dialog;
  public string dialogMean;
  public string dialogEvil;
  public Dictionary<int, int[]> emphasedWords;
  public int character;

  public string[] dialogArray;
  public string[] meanDialogArray;
  public string[] evilDialogArray;


  public void SetCurrentLayer(int layer) {
    currentLayer = layer;
  }

  public void SetDialogArray()
  {
    dialogArray = dialog.Split(new char[] { ' ', '​' });
    meanDialogArray = dialogMean.Split(new char[] { ' ', '​' });
    evilDialogArray = dialogEvil.Split(new char[] { ' ', '​' });
  }

  string GetOpenEmphase()
  {
    string result;
    switch(currentLayer)
    {
      case 0:
        result = "";
        break;
      case 1:
        result = "<i>";
        break;
      case 2:
        result = "<color=\"red\">";
        break;
      case 3:
        result = "<color=\"red\">";
        break;
      default:
        result = "";
        break;
    }
    return result;
  }

  string GetCloseEmphase()
  {
    string result = "";
    switch (currentLayer)
    {
      case 0:
        break;
      case 1:
        result = "</i>";
        break;
      case 2:
        result = "</color>";
        break;
      case 3:
        result = "</color>";
        break;
      default:
        result = "";
        break;
    }
    return result;
  }

  public string DisplayDialog() {
    string[] currentDialog;
    if (currentLayer<3)
    {
      currentDialog = dialogArray;
    }
    else if(currentLayer>3)
    {
      currentDialog = evilDialogArray;
    }
    else
    {
      currentDialog = meanDialogArray;
    }
    string result = "";
    string openEmphasedWord = GetOpenEmphase();
    string closeEmphasedWord = GetCloseEmphase();
    int[] wordsToEmphase = emphasedWords[currentLayer];
    for(int i = 0; i < currentDialog.Length; i++) {
      if(wordsToEmphase.Contains(i)) {
        result += openEmphasedWord + currentDialog[i] + closeEmphasedWord + " ";
      } else {
        result += currentDialog[i] + " " ;
      }
    }
    return result;
  }

}
