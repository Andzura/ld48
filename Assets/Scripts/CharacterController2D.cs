using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController2D : MonoBehaviour
{
  public float jumpForce = 100f;
  public float speedFactor = 500f;
  public float moveSmoothing = 0.12f;
  public float crouchFactor = 0.6f;
  public Collider2D crouchDisableCollider;
  public Transform crouchCheck;
  public Transform groundCheck;
  public SoundManager soundManager;
  public LayerMask notPlayerLayers;
  private Rigidbody2D rb;
  private Vector3 velocity = Vector3.zero;
  private bool onGround;
  private float overlapRadius = 0.2f;
  private bool facingRight = true;

  public bool hidden = false;

  private Animator animator;
  private SpriteRenderer spriteRenderer;

  public LayersController layersController;
  private int currentLayer;
  private int previousLayer;

  void GetCurrentAnimator() {
    if (animator != null) {
      ResetAnimator();
    }
    if (spriteRenderer != null) {
      spriteRenderer.enabled = false;
    }
    if(currentLayer > 4)
      currentLayer = 4;
    animator = GameObject.Find($"PlayerSpriteLayer{currentLayer}").GetComponent<Animator>();
    CleanAnimator();
    spriteRenderer = GameObject.Find($"PlayerSpriteLayer{currentLayer}").GetComponent<SpriteRenderer>();
    spriteRenderer.enabled = true;
    animator.enabled = true;
  }

  public void SetFocus(bool focus)
  {
    animator.SetBool("Focus", focus);
  }
  public Animator GetAnimator() {
    return animator;
  }

  public void ResetAnimator() {
    animator.enabled = false;
    CleanAnimator();
  }

  public void CleanAnimator() {
    animator.SetBool("Hide", false);
    animator.SetBool("JumpUp", false);
    animator.SetBool("JumpDown", false);
    animator.SetBool("Hit", false);
    animator.SetBool("Walk", false);
    animator.SetBool("Focus", false);
  }

  // Start is called before the first frame update
  void Awake() {
    rb = GetComponent<Rigidbody2D>();
    currentLayer = layersController.currentLayer;
    GetCurrentAnimator();
  }

  // Frame-rate independent Update, called every Time.fixedDeltaTime
  void FixedUpdate() {
    currentLayer = layersController.currentLayer;
    if (currentLayer != previousLayer)
    {
      hidden = false;
    }
    if (currentLayer != previousLayer && currentLayer != 5) {
      GetCurrentAnimator();
      previousLayer = currentLayer;
    } else if (currentLayer != previousLayer && currentLayer == 5) {
      animator.SetBool("Hit", true);
      previousLayer = currentLayer;
    }
    bool prev_onGround = onGround;
    onGround = false;
    Collider2D[] colliders = Physics2D.OverlapCircleAll(groundCheck.position, overlapRadius, notPlayerLayers);
    for (int i = 0; i < colliders.Length; i++)
    {
      if (colliders[i].gameObject != gameObject)
      {
        if(!prev_onGround)
        {
          soundManager.PlaySound(SoundManager.Sound.PlayerLand, false, false);
          if(animator.GetBool("Walk"))
          {
            soundManager.PlaySound(SoundManager.Sound.PlayerRun, true, false);
          }
        }
        onGround = true;
      }
    }
  }
  public void Move(float move, bool jump, bool crouch) {
    if (currentLayer != previousLayer) {
      GetCurrentAnimator();
      previousLayer = currentLayer;
    }
    bool prev_walk = animator.GetBool("Walk");
    if(hidden) {
      animator.SetBool("Hide", true);
    } else {
      animator.SetBool("Hide", false);
    }
    if (move != 0) {
      animator.SetBool("Walk", true);
      if (!prev_walk)
      {
        soundManager.PlaySound(SoundManager.Sound.PlayerRun, true, false);
      }
    } else {
      animator.SetBool("Walk", false);
      if (prev_walk)
      {
        soundManager.StopSound(SoundManager.Sound.PlayerRun);
      }
    }

    if(onGround) {
      Debug.Log("Grounded");
      animator.SetBool("JumpUp", false);
      animator.SetBool("JumpDown", false);
    } else if (rb.velocity.y > -0.1) {
      animator.SetBool("JumpUp", true);
      animator.SetBool("JumpDown", false);
    } else if (rb.velocity.y < -0.1) {
      animator.SetBool("JumpUp", false);
      animator.SetBool("JumpDown", true);
    }

    if(!onGround)
    {
      soundManager.StopSound(SoundManager.Sound.PlayerRun);
    }

    //set velocity to RB
    Vector3 targetVelocity = new Vector2(move * speedFactor, rb.velocity.y);
    rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref velocity, moveSmoothing);
			if ((move > 0 && !facingRight) || (move < 0 && facingRight))
				Flip();
    //apply Jump
    if(onGround && jump){
      soundManager.PlaySound(SoundManager.Sound.PlayerJump, false, false);
      onGround = false;
      rb.AddForce(new Vector2(0f, jumpForce), ForceMode2D.Impulse);
    }
  }

  void Flip(){
    // Switch the way the player is labelled as facing.
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
  }

  //If we want some horizontally moving platforms, we need this + debugs some trucs de merde
  //void OnCollisionEnter2D(Collision2D collision)
  //{
  //  if (collision.gameObject.name.Equals("MovingPlatform"))
  //    this.transform.parent = collision.transform;
  //}

  //void OnCollisionExit2D(Collision2D collision)
  //{
  //  if (collision.gameObject.name.Equals("MovingPlatform"))
  //    this.transform.parent = null;
  //}
}
