using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour {

  public CharacterController2D characterController;
  public LayersController layersController;
  float horizontal = 0;
  bool jump = false;
  bool crouch = false;
  bool deeper = false;
  bool upper = false;
  bool death = false;
  bool respawn = false;
  bool useObject = false;

  public bool allowDeep = true;
  public bool allowUp = true;
  public bool allowFocus = true;

  //Focus key
  bool focus = false;
  float focusTime = 0f;
  float requiredFocusTime = 2f;

  // Focusing to decrease insanity
  public bool focusing = false;
  public CheckpointsController checkpointsController;
  public bool block = false;

  void Awake() {
  }

  void Update () {
    horizontal = Input.GetAxisRaw("Horizontal");
    if (Input.GetButtonDown("Jump"))
    {
      jump = true;
    }
    crouch = Input.GetButton("Crouch");

    if(Input.GetButtonDown("Deeper")){
      deeper = true;
    }
    if(Input.GetButtonDown("Upper")){
      upper = true;
    }
    if (Input.GetButtonDown("Focus")) {
      Debug.Log("je commence à focus !");
    }
    if (Input.GetButton("Focus")) {
      focus = true;
    }
    if (Input.GetButtonUp("Focus")) {
      Debug.Log("j'arrete le focus !");
      focus = false;
    }

    if(Input.GetButtonDown("Death")){
      death = true;
    }
    if(Input.GetButtonDown("Respawn")){
      respawn = true;
    }
    if (Input.GetButtonDown("UseObject")) {
        useObject = true;
    }
  }

  void FixedUpdate() {
    if (allowFocus) {
      GetComponent<Player>().SetFocus(focus);
    }
    if(!block){
      if (!focus) {
        characterController.Move(horizontal * Time.fixedDeltaTime, jump, crouch);
        if(deeper && allowDeep){
          layersController.goDeeper();
        }else if(upper && allowUp){
          layersController.goUp();
        }
        if(death){
           GetComponent<Player>().Die();
        }
        if(respawn){
          transform.position = checkpointsController.getRespawnPoint(transform.position);
        }
        if(useObject) {
          GetComponent<Player>().UseObject();
        }
      }
    }
    deeper = false;
    upper = false;
    death = false;
    respawn = false;
    jump = false;
    useObject = false;
  }
  public void BlockInput(){
    block = true;
  }

  public void AllowInput(){
    block = false;
  }
}