using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Layer : MonoBehaviour
{
  public int layer_depth;
  // Start is called before the first frame update
  void Start() {

  }

  // Update is called once per frame
  void Update() {
      
  }

  public void DeActivate(){
    //#TODO: disable/hide all children, I guess
    foreach(Transform children in transform){
      children.gameObject.SetActive(false);
    }
  }

  public void Activate(){
    //#TODO: enable/show all children
    foreach(Transform children in transform){
      children.gameObject.SetActive(true);
    }
  }

}
