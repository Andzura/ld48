using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
  public bool tutorialOn = true;
  public bool tutorialDone = false;
  public Player player;

  public LayersController layersController;
  public InputController inputController;
  public CharacterController2D characterController;
  private Vector3 distanceToPlayer = Vector3.zero;

  private float remainingTime = 3f;

  public Npc prudence;

  //0 : before tutorial is triggered
  //1 : display first dialog
  private int step = 0;

  void Awake() {

  }

  public void FixedUpdate() {
    if (((player.transform.position - transform.position).magnitude < 2f) && !tutorialOn && !tutorialDone) {
      tutorialOn = true;
      step = 1;
      Animator animator = characterController.GetAnimator();
      animator.SetBool("Hide", false);
      animator.SetBool("JumpUp", false);
      animator.SetBool("JumpDown", false);
      animator.SetBool("Hit", false);
      animator.SetBool("Walk", false);
      characterController.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
    }
    if (step == 1) {
      inputController.block = true;
      prudence.ComeToYou();
      if (prudence.nextToYou) {
          step = 2;
          prudence.Flip();
      }
    } else if (step == 2) {
      inputController.allowFocus = true;
      inputController.allowDeep = false;
      prudence.transform.Find("DialogBox1").gameObject.SetActive(true);
      if(layersController.currentLayer == 3) {
        step = 3;
      }
    } else if (step == 3) {
      prudence.transform.Find("DialogBox1").gameObject.SetActive(false);
      prudence.transform.Find("DialogBox2").gameObject.SetActive(true);
      if(layersController.currentLayer == 2) {
        step = 4;
      }
    } else if (step == 4) {
      prudence.transform.Find("DialogBox2").gameObject.SetActive(false);
      prudence.transform.Find("DialogBox3").gameObject.SetActive(true);
      if(layersController.currentLayer == 1) {
        step = 5;
      }
    } else if (step == 5) {
      prudence.transform.Find("DialogBox3").gameObject.SetActive(false);
      prudence.transform.Find("DialogBox4").gameObject.SetActive(true);
      if(layersController.currentLayer == 0) {
        step = 6;
      }
    } else if (step == 6) {
      prudence.transform.Find("DialogBox4").gameObject.SetActive(false);
      prudence.transform.Find("DialogBox5").gameObject.SetActive(true);
      step = 7;
    } else if (step == 7) {
      remainingTime -= Time.deltaTime;
      if (remainingTime <= 0){
        prudence.transform.Find("DialogBox5").gameObject.SetActive(false);
        prudence.transform.Find("DialogBox6").gameObject.SetActive(true);
        remainingTime = 5f;
        step = 8;
      }
    } else if (step == 8) {
      remainingTime -= Time.deltaTime;
      if (remainingTime <= 0){
        prudence.transform.Find("DialogBox6").gameObject.SetActive(false);
        step = -1;
        tutorialDone = true;
        inputController.block = false;
        inputController.allowDeep = true;
      }
    }
  }
}