using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class EnemyBehaviour : MonoBehaviour
{

	private int currentLayer;
  private int previousLayer;
  public LayersController layerController;
  public KillZone killzone;

	private Rigidbody2D rb;
  private bool facingRight = false;

	public Transform player;
  public Player playerObject;
	public Transform topWallCheck;
	public Transform bottomWallCheck;
	public float speedFactor = 100f;
	public float followFlyDistance = 15f;
	public float followWalkDistance = 15f;
  public float maxDistanceFromHome = 10f;
	public LayerMask notPlayerLayers;
	private Vector3 distanceToPlayer = Vector3.zero;
	private Vector3 distanceToOrigin = Vector3.zero;
	private Vector3 origin = Vector3.zero;
	private float overlapRadius = 0.01f;
	private float forwardVelocity = 1f;

  private Animator animator;
  private SpriteRenderer spriteRenderer;


  void GetCurrentAnimator() {
    if (animator != null) {
      animator.enabled = false;
      animator.SetBool("Moving", false);
    }
    if (spriteRenderer != null) {
      spriteRenderer.enabled = false;
    }
    if(currentLayer > 4)
      currentLayer = 4;
    //Debug.Log($"layer is {currentLayer}");
    animator = transform.Find($"EnemySpriteLayer{currentLayer}").GetComponent<Animator>();
    spriteRenderer = transform.Find($"EnemySpriteLayer{currentLayer}").GetComponent<SpriteRenderer>();
    spriteRenderer.enabled = true;
    animator.enabled = true;
  }
	void Awake() {
		rb = GetComponent<Rigidbody2D>();
		origin = transform.position;
    currentLayer = layerController.currentLayer;
    GetCurrentAnimator();
	}

	// Start is called before the first frame update
	void Start()
	{
    Physics.IgnoreLayerCollision(8, 8);
  }

	// Update is called once per frame
	void Update() {


	}

  void Flip(){
    // Switch the way the player is labelled as facing.
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
    forwardVelocity *= (-1f);
  }


  void FixedUpdate() {
    currentLayer = layerController.currentLayer;
    if (currentLayer != previousLayer) {
      GetCurrentAnimator();
      transform.position = origin;
      rb.velocity = Vector2.zero;
      previousLayer = currentLayer;
      rb.velocity = Vector2.zero;
      if(facingRight)
      {
        Flip();
      }
    }
    HandleUntouchablePlayer();
    Invoke($"ActLayer{currentLayer}", 0);
  }

  void HandleUntouchablePlayer() {
    if(playerObject.GetComponent<CharacterController2D>().hidden || layerController.currentLayer == 0) {
      Physics2D.IgnoreCollision(transform.GetComponent<Collider2D>(), player.GetComponent<BoxCollider2D>(), true);
    } else {
      Physics2D.IgnoreCollision(transform.GetComponent<Collider2D>(), player.GetComponent<BoxCollider2D>(), false);
    }
  }

  void ActLayer0() {
    rb.gravityScale = 0f;
    killzone.active = false;
  }
  void ActLayer1() {
    killzone.active = true;
    LookTowardPlayer();
  }
  void ActLayer2() {
    killzone.active = true;
    WalkForward();
	}
	void ActLayer3()
	{
    killzone.active = true;
		WalkTowardPlayer();
	}
	void ActLayer4() {
    killzone.active = true;
    FlyTowardPlayer();
  }
  void ActLayer5() {
    killzone.active = false;
    Debug.Log("UltimateLayer");
  }
  void ActLayer6() {
    killzone.active = false;
    Debug.Log("HappyLayer");
  }


	void WalkForward()
	{
    rb.gravityScale = 3f;
    animator.SetBool("Moving", true);
		RaycastHit2D collider_top = Physics2D.Raycast(topWallCheck.position,Vector2.left, overlapRadius,notPlayerLayers);
    if (collider_top.collider != null)
    {
      Flip();
    }
    RaycastHit2D collider_bottom = Physics2D.Raycast(bottomWallCheck.position,Vector2.left, overlapRadius,notPlayerLayers);
    if (collider_bottom.collider != null)
    {
      Flip();
    }
		//Collider2D[] colliders_right = Physics2D.OverlapCircleAll(rightWallCheck.position, overlapRadius, notPlayerLayers);
		//for (int i = 0; i < colliders_right.Length; i++)
		//{
		//	if (colliders_right[i].gameObject != gameObject)
  //    {
  //      Debug.Log("switch -");
  //      forwardVelocity = (1f);
  //      if(facingRight) {
  //        Flip();
  //      }
		//	}
		//}
		Vector2 targetVelocity = new Vector2(Vector3.left.x * speedFactor * forwardVelocity * 0.8f,rb.velocity.y);
		rb.velocity = targetVelocity;
	}

	void WalkTowardPlayer()
  {
    rb.gravityScale = 3f;
    distanceToPlayer = (player.position - transform.position);
    bool isPlayerLeft = player.position.x - transform.position.x < 0;
    distanceToOrigin = (transform.position - origin);
    bool isOriginLeft = origin.x - transform.position.x < 0;
    if ((distanceToPlayer.magnitude < followWalkDistance) && (distanceToOrigin.magnitude < 15) && !playerObject.GetComponent<CharacterController2D>().hidden)
    {
      if (isPlayerLeft && facingRight)
      {
        Flip();
      }
      else if (!isPlayerLeft && !facingRight)
      {
        Flip();
      }
      animator.SetBool("Moving", true);
			Vector3 velocity = Vector3.zero;
			Vector3 targetVelocity = new Vector3(Mathf.Sign(distanceToPlayer.x) * speedFactor * 3f, rb.velocity.y, 0.0f);
			rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref velocity, 0.2f);
      if (Mathf.Sign(distanceToPlayer.x) > 0 && !facingRight) {
        Flip();
        //forwardVelocity *= (-1f);
      } else if (Mathf.Sign(distanceToPlayer.x) <= 0 && facingRight) {
        Flip();
        //forwardVelocity *= (-1f);
      }
		}
		else
    {
      if (isOriginLeft && facingRight)
      {
        Flip();
      }
      else if (!isOriginLeft && !facingRight)
      {
        Flip();
      }
      distanceToPlayer = (origin - transform.position);
			if (Mathf.Abs(distanceToPlayer.x) > 1.0)
			{
        animator.SetBool("Moving", true);
				distanceToPlayer.Normalize();
				Vector3 velocity = Vector3.zero;
				Vector3 targetVelocity = new Vector3(Mathf.Sign(distanceToPlayer.x) * speedFactor * 3f, rb.velocity.y, 0.0f);
				rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVelocity, ref velocity, 0.2f);
        //if (Mathf.Sign(distanceToPlayer.x) > 0 && !facingRight) {
        //  Flip();
        //  //forwardVelocity *= (-1f);
        //} else if (Mathf.Sign(distanceToPlayer.x) <= 0 && facingRight) {
        //  Flip();
        //  //forwardVelocity *= (-1f);
        //}
			}
		}
	}

  void FlyTowardPlayer() {
		rb.gravityScale = 0f;
		distanceToPlayer = (player.position - transform.position);
    bool isPlayerLeft = player.position.x - transform.position.x < 0;
    if (isPlayerLeft && facingRight) {
      Flip();
    } else if (!isPlayerLeft && !facingRight) {
      Flip();
    }
		distanceToOrigin = (transform.position - origin);
		if ((distanceToPlayer.magnitude < followFlyDistance) && (distanceToOrigin.magnitude<maxDistanceFromHome) && !playerObject.GetComponent<CharacterController2D>().hidden)
		{
			distanceToPlayer.Normalize();
      animator.SetBool("Moving", true);
			rb.AddForce(distanceToPlayer * speedFactor*3f);
		}
		else
		{
			distanceToPlayer = (origin - transform.position);
			if (distanceToPlayer.magnitude > 1.0)
			{
				distanceToPlayer.Normalize();
				rb.AddForce(distanceToPlayer * speedFactor*3f);
        animator.SetBool("Moving", true);
			}
		}
	}

  void LookTowardPlayer() {
    rb.gravityScale = 0f;
  }

}
