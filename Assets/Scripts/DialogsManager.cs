using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;
using Newtonsoft.Json;


public class DialogsManager : MonoBehaviour
{
  public Dictionary<int, Dialog> dialogs = new Dictionary<int, Dialog>();
  public LayersController layersController;
  public Font fontRegular;
  public Font fontEvil;

  private IEnumerator ImportDialog() {
    ///* // WebGl
    UnityWebRequest uwr = UnityWebRequest.Get(System.IO.Path.Combine(Application.streamingAssetsPath,"dialogs/DialogsManifest.txt"));
    yield return uwr.SendWebRequest();
    Debug.Log("UWR done");
    if (uwr.isNetworkError || uwr.isHttpError)
        {
            if (uwr.isNetworkError)
            {
                Debug.Log("UWR network error: " + uwr.error);
            }
            else if (uwr.isHttpError)
            {
                Debug.Log("UWR http error: " + uwr.error);
            }
            else
            {
                Debug.Log("UWR streaming error: " + uwr.error);
            }
        }
        else
        {
            if (uwr.isDone)
            {
                Debug.Log("UWR OK");
                string text = uwr.downloadHandler.text;
                char[] delims = new[] { '\r', '\n' };
                string[] strings = text.Split(delims);
                foreach(string filename in strings) {
                  if(string.IsNullOrEmpty(filename)){
                    continue;
                  }
                  Debug.Log(filename);
                  StartCoroutine(ParseDialogFileWeb("dialogs/"+filename));
                }
            }
        }
    //*/
    /* //Windows 
    DirectoryInfo dir = new DirectoryInfo(Application.streamingAssetsPath + "/dialogs/");
    FileInfo[] files = dir.GetFiles("*.json");
    foreach(FileInfo file in files) {
      Debug.Log(file);
      ParseDialogFile(file);
    } */
  }

  private IEnumerator ParseDialogFileWeb(string filename) {
    using (UnityWebRequest uwr = UnityWebRequest.Get(System.IO.Path.Combine(Application.streamingAssetsPath,filename))){
      yield return uwr.SendWebRequest();
      Debug.Log("OK UWR PARSE");
      if (uwr.isNetworkError || uwr.isHttpError)
          {
              if (uwr.isNetworkError)
              {
                  Debug.Log("UWR network error: " + uwr.error);
              }
              else if (uwr.isHttpError)
              {
                  Debug.Log("UWR http error: " + uwr.error);
              }
              else
              {
                  Debug.Log("UWR streaming error: " + uwr.error);
              }
          }
          else
          {
              if (uwr.isDone)
              {
                  string json = uwr.downloadHandler.text;
                  Debug.Log(json);
                  Dialog dialog = JsonConvert.DeserializeObject<Dialog>(json);
                  dialog.SetDialogArray();
                  dialogs.Add(dialog.character, dialog);
              }
          }
    }
  }

  private void ParseDialogFile(FileInfo file) {
    StreamReader reader = new StreamReader(file.OpenRead());
    string json = reader.ReadToEnd();
    Dialog dialog = JsonConvert.DeserializeObject<Dialog>(json);

    dialog.SetDialogArray();
    dialogs.Add(dialog.character, dialog);
  }

  void Start()
  {
    StartCoroutine(ImportDialog());
  }

  public string DisplayDialog(int character) {
    Debug.Log(character);
    Debug.Log(dialogs.Keys);
    Dialog dialog = dialogs[character];
    Debug.Log(dialogs);
    dialog.SetCurrentLayer(layersController.currentLayer);
    return dialog.DisplayDialog();
  }

  // Update is called once per frame
  void Update()
  {
  }
}
