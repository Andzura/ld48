using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HidingGate : MonoBehaviour
{

  public Transform playerTransform;

  public Player player;

  private CharacterController2D characterController2D;

  private Vector3 distanceToPlayer = Vector3.zero;

  private float hiddenDistance = 1.15f;

  private bool hidden = false;


  // Start is called before the first frame update

  void Awake() {
    characterController2D = player.GetComponent<CharacterController2D>();
  }
  void Start()
  {

  }

  // Update is called once per frame
  void FixedUpdate()
  {
    distanceToPlayer = (playerTransform.position - transform.position);
    if(hidden)
      Debug.Log(distanceToPlayer.magnitude);
    if ((distanceToPlayer.magnitude < hiddenDistance) && !hidden) {
      Hide();
    } else if ((distanceToPlayer.magnitude >= hiddenDistance) && hidden) {
      UnHide();
    }
  }

  void Hide() {
    Debug.Log("je suis cachée");
    characterController2D.hidden = true;
    hidden = true;
  }

  void UnHide() {
    Debug.Log("je suis pas cachée");
    characterController2D.hidden = false;
    hidden = false;
  }
}
