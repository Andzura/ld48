using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public RectTransform playableArea;
    public Transform following;
    public float smoothingFactor;
    public float tolerance;
    public bool overrideArea = false;
    private Vector3 position;
    private float aspectRatio = 16f/9f;
    private Camera camera;
    private float halfHeight;
    private float halfWidth;
    // Start is called before the first frame update
    void Start()
    {
        camera = GetComponent<Camera>();
        if(camera != null){
          halfHeight = camera.orthographicSize;
          halfWidth = halfHeight * aspectRatio;
          Debug.Log("HalfHeight:"+halfHeight);
          Debug.Log("HalfWidth:"+halfWidth);
          Debug.Log("aspectRatio:" + aspectRatio);
        }
    }

    // Update is called once per frame
    void FixedUpdate() {
      Vector3 pos = transform.position;
      Vector3 target = CorrectedPos();
      target.z = pos.z;
      if((target  - pos).magnitude > tolerance) {
        transform.position = Vector3.SmoothDamp(pos, target, ref position, smoothingFactor);
      }
    }

    private Vector2 CorrectedPos(){
      Vector2 correctedPos = following.position;
      if(playableArea != null && !overrideArea){
        float xMax = playableArea.position.x +  playableArea.rect.xMax;
        float xMin = playableArea.position.x + playableArea.rect.xMin;
        float yMax = playableArea.position.y +  playableArea.rect.yMax;
        float yMin = playableArea.position.y + playableArea.rect.yMin;
        Rect rect = Rect.MinMaxRect(xMin, yMin, xMax, yMax);
        if(!rect.Contains(correctedPos+new Vector2(-halfWidth,0))){
          correctedPos.x += rect.xMin - (correctedPos+new Vector2(-halfWidth,0)).x;
        }
        if(!rect.Contains(correctedPos+new Vector2(halfWidth,0))){
          correctedPos.x += rect.xMax - (correctedPos+new Vector2(halfWidth,0)).x;
        }
        if(!rect.Contains(correctedPos+new Vector2(0,-halfHeight))){
          correctedPos.y += rect.yMin - (correctedPos+new Vector2(0,-halfHeight)).y;
        }
        if(!rect.Contains(correctedPos+new Vector2(0,halfHeight))){
          correctedPos.y += rect.yMax - (correctedPos+new Vector2(0,halfHeight)).y;
        }
      }
      return correctedPos;
    } 
}
