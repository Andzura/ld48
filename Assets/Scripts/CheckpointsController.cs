using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointsController : MonoBehaviour
{
    public List<Checkpoint> checkpoints;

    void Awake(){
        foreach(Transform child in transform){
          checkpoints.Add(child.gameObject.GetComponent<Checkpoint>());
        }
    }

    public Vector2 getRespawnPoint(Vector2 position) {
      foreach(Checkpoint checkpoint in checkpoints){
        if(checkpoint.Contains(position)){
          return checkpoint.respawn.position;
        }
      }
      return Vector2.zero;
    }
}
