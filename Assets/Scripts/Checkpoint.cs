using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public Transform respawn;
    private RectTransform zone;
    
    void Awake(){
      zone = GetComponent<RectTransform>();
    }
    public bool Contains(Vector2 pos){
      return zone.rect.Contains(pos-((Vector2)zone.position));
    }
}
