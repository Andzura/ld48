using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
  public SoundAudioClip[] SoundAudioClipArray;
  private List<AudioSource> audioSources = new List<AudioSource>();
  private Dictionary<AudioSource, Coroutine> audioCoroutineMap = new Dictionary<AudioSource, Coroutine>();

  [System.Serializable]
  public class SoundAudioClip
  {
    public SoundManager.Sound sound;
    public AudioClip audioClip;
  }

  public enum Sound
  {
    PlayerRun,
    PlayerJump,
    PlayerLand,
    PlayerHit,
    PlayerEatConsumable,
    EnemyWalk,
    EnemyRun,
    EnemyFly,
    LayerChange
  }


  public void PlaySound(Sound sound, bool loop, bool multipleSounds)
  {
    //Debug.Log($"Sound{sound}");
    if (!multipleSounds)
    {
      if(transform.Find($"Sound{sound}")!=null)
      {
        AudioSource existingSource = transform.Find($"Sound{sound}").GetComponent<AudioSource>();
        existingSource.loop = loop;
        if(existingSource.isPlaying)
        {
          if(audioCoroutineMap.ContainsKey(existingSource) && audioCoroutineMap[existingSource]!=null)
          {
            StopCoroutine(audioCoroutineMap[existingSource]);
          }
          audioCoroutineMap[existingSource] = StartCoroutine(AudioFadeOut.FadeIn(existingSource, 0.2f, 1.0f));
        }
        else
        {
          existingSource.Play();
        }
        return;
      }
    }
    GameObject soundGameObject = new GameObject($"Sound{sound}");
    AudioSource audioSource = soundGameObject.AddComponent<AudioSource>();
    audioSource.transform.SetParent(transform);
    audioSource.loop = loop;
    audioSource.clip = GetAudioClip(sound);
    audioSource.Play();
  }


  public void StopSound(Sound sound)
  {
    if (transform.Find($"Sound{sound}") != null)
    {
      AudioSource existingSource = transform.Find($"Sound{sound}").GetComponent<AudioSource>();
      if(!existingSource.isPlaying)
      {
        return;
      }
      if (audioCoroutineMap.ContainsKey(existingSource) && audioCoroutineMap[existingSource] != null)
      {
        StopCoroutine(audioCoroutineMap[existingSource]);
      }
      audioCoroutineMap[existingSource] = StartCoroutine(AudioFadeOut.FadeOut(existingSource,0.2f));
    }
    return;
  }

  private AudioClip GetAudioClip(Sound sound)
  {
    foreach (SoundAudioClip soundAudioClip in SoundAudioClipArray)
    {
      if (soundAudioClip.sound == sound)
      {
        return soundAudioClip.audioClip;
      }
    }
    Debug.LogError("Ca a merd� frer: son " + sound + " introuvable");
    return null;
  }
}
