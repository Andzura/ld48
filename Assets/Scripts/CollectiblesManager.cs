using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectiblesManager : MonoBehaviour
{
  public int collectiblesCounter = 0;
  public UnityEngine.UI.Text text;
  // Start is called before the first frame update
  void Start()
  {
    //text.text = "x0";
  }

  // Update is called once per frame
  void Update()
  {
    string newtext = "x" + collectiblesCounter.ToString();
    //text.text = newtext;
  }

  public void GetCollectible() {
    collectiblesCounter ++;
    Debug.Log($"you have {collectiblesCounter} items");
  }

  public void UseCollectible() {
    if(collectiblesCounter > 0) {
      collectiblesCounter --;
      Debug.Log($"you have {collectiblesCounter} items");
    }
  }

  public bool HasObject() {
    return collectiblesCounter > 0 ? true : false;
  }
}
