using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
  public float moveSpeed = 1f, from = 0f, to = 2f;
  public enum Direction
  {
    X,
    Y
  }

  public Direction direction;
    // Update is called once per frame
  void FixedUpdate()
  {
    if(direction == Direction.Y)
    {
      if (transform.position.y > to)
      {
        transform.position = new Vector2(transform.position.x, from);
      }
      transform.position = new Vector2(transform.position.x, transform.position.y + moveSpeed * 0.01f + Time.deltaTime);
    }
    else
    {
      if (transform.position.x > to)
      {
        transform.position = new Vector2(from, transform.position.y);
      }
      transform.position = new Vector2(transform.position.x + moveSpeed * 0.01f + Time.deltaTime, transform.position.y);
    }
  }
}
